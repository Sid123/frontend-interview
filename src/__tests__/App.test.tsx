import React from 'react';
import { fireEvent, getByTestId, render, screen, getByText } from '@testing-library/react';
import App from '../App';

test('renders learn react link', () => {
  const comp = render(<App />);
  expect(comp).toBeTruthy();
});

test("Test Action button click", () => {
  const {container} = render(<App />);

  const stopButton = getByText(container, 'Stop');
  const clearButton = getByText(container, 'Clear All');
  fireEvent.click(stopButton);
  fireEvent.click(clearButton);
});

test("Test info Row exist", () => {
  const {container} = render(<App />);

  const InfoRow = getByTestId(container, 'inforow');
  expect(InfoRow).toBeTruthy()

});

test("Test Error Row exist", () => {
  const {container} = render(<App />);

  const ErrorRow = getByTestId(container, 'errorRow');
  expect(ErrorRow).toBeTruthy()

});

test("Test Warning Row exist", () => {
  const {container} = render(<App />);

  const WarningRow = getByTestId(container, 'warningRow');
  expect(WarningRow).toBeTruthy()

});