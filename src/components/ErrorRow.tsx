import { TableRow, TableCell } from '@material-ui/core';
import React from 'react'
import styled from 'styled-components';

interface Message {
  message: any,
  priority: any,
}

interface AppProps {
  msg: Message;
  clearRecord: Function
}


const CustomErrorTableRow = styled.div`
  margin: 10px;
  flex: 1;
  flex-basis: 410px;
  position: relative;
  background-color: #F56236;
  min-width: 300px;
`;

const ClearText = styled.small`
  position: absolute;
  flex: 1;
  bottom: 10px;
  z-index: 9999;
  right: 20px
`;

export default function ErrorRow({ clearRecord, msg }: AppProps) {
  return (
    <CustomErrorTableRow>
      <TableRow style={{ padding: 120, }} key={msg.message}>
        <TableCell component="th" scope="row">
          {msg.message}
        </TableCell>
      </TableRow>
      <ClearText onClick={() => clearRecord(msg)}>
        Clear
      </ClearText>
    </CustomErrorTableRow>
  )
}
