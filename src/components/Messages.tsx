import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow'
import styled from 'styled-components';
// @ts-ignore
import { countBy } from 'lodash';
import ErrorRow from './ErrorRow';
import WarningRow from './WarningRow';
import InfoRow from './InfoRow';
import Header from './Header';

interface AppProps {
  message: Array<object>;
  clearMessages: Function
  setstopMessages: Function;
  stopMessages: boolean
}

const CountText = styled.div`
  margin: 5px;
  margin-left: 20px;
`;

const MainContanier = styled.div`
   display: flex;
   flex-direction: row;
`;

const MessageRow = styled.div`
   display: flex;
   flex-direction: column;
   padding: 10px
`;

const TopButtonComponent = styled.div`
   display: flex;
   flex-direction: row;
   margin: auto;
   align-items: center;
   align-self: center;
   justify-content: center;
   padding: 10px
`;

const ActionButtons = styled.button`
    margin: 15px;
    background-color: #88FCA3;
`;

export default function Messages({ message, clearMessages, setstopMessages, stopMessages }: AppProps) {

  const clearRecord = (msg: any) => {
    let index = message.indexOf(msg);
    message.splice(index, 1)
  }

  const handleAction = (action: any) => {
    if (action === 'stop') {
      setstopMessages()
    } else {
      clearMessages()
    }
  }

  const ErrorCount = countBy(message, (msg: any) => msg.priority === 0)['true'] || 0;
  const WarningCount = countBy(message, (msg: any) => msg.priority === 2)['true'] || 0;
  const InfoCount = countBy(message, (msg: any) => msg.priority === 1)['true'] || 0;

  return (
    <div>
      <Header/>
      <TopButtonComponent>
        <ActionButtons id='actionButton1' onClick={() => handleAction('stop')}>
          {stopMessages ? 'Start' : 'Stop'}
        </ActionButtons>
        <ActionButtons id='actionButton2' onClick={() => handleAction('clear')}>
          Clear All
        </ActionButtons>
      </TopButtonComponent>
      <Table aria-label="simple table" style={{border: '1px solid black'}}>
        <MainContanier>
          <MessageRow data-testid='errorRow'>
            <TableRow>
              <TableCell>Error Type 1</TableCell>
              <CountText> {`Count ${ErrorCount}`} </CountText>
            </TableRow>
            <TableBody>
              {message.filter((item: any) => item.priority === 0).map((msg: any) => (
                <ErrorRow clearRecord={clearRecord} msg={msg}/>
              ))}
            </TableBody>
          </MessageRow>
          <MessageRow data-testid='warningRow'>
            <TableRow>
              <TableCell>Warning Type 2</TableCell>
              <CountText> {`Count ${WarningCount}`} </CountText>
            </TableRow>
            <TableBody>
              {message.filter((item: any) => item.priority === 1).map((msg: any) => (
                <WarningRow clearRecord={clearRecord} msg={msg} />
              ))}
            </TableBody>
          </MessageRow>
          <MessageRow data-testid='inforow'>
            <TableRow>
              <TableCell>Info Type 3</TableCell>
              <CountText> {`Count ${InfoCount}`} </CountText>
            </TableRow>
            <TableBody>
              {message.filter((item: any) => item.priority === 2).map((msg: any) => (
                <InfoRow clearRecord={clearRecord} msg={msg} />
              ))}
            </TableBody>
          </MessageRow>
        </MainContanier>
      </Table>
    </div>
  )
}
