import React from 'react'
import { Button, Snackbar } from '@material-ui/core'

interface AppProps {
  ErrorMsg: any;
  open: boolean;
  setOpen: Function
}


export default function SnakBar({ open, ErrorMsg, setOpen }: AppProps) {
  
  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  return (
    <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message="Note archived"
        action={
          <React.Fragment>
            <strong>
              { ErrorMsg.message }
            </strong>
            <Button color="secondary" size="small" onClick={handleClose}>
              UNDO
            </Button>
          </React.Fragment>
        }
      />
  )
}
