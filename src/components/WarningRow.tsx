import { TableRow, TableCell } from '@material-ui/core';
import React from 'react'
import styled from 'styled-components';

interface Message {
  message: any,
  priority: any,
}

interface AppProps {
  msg: Message;
  clearRecord: Function
}


const CustomWarningTableRow = styled.div`
  margin: 10px;
  flex: 1;
  flex-basis: 410px;
  position: relative;
  background-color: #FCE788;
  min-width: 300px;
`;

const ClearText = styled.small`
  position: absolute;
  flex: 1;
  bottom: 10px;
  z-index: 9999;
  right: 20px
`;

export default function WarningRow( { clearRecord, msg }: AppProps) {
  return (
    <CustomWarningTableRow key={msg.message}>
    <TableRow>
      <TableCell component="th" scope="row">
        {msg.message}
      </TableCell>
    </TableRow>
    <ClearText onClick={() => clearRecord(msg)} className='clearButton'>
      Clear
    </ClearText>
  </CustomWarningTableRow>
  )
}
