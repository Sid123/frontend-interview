import { TableRow, TableCell } from '@material-ui/core';
import React from 'react'
import styled from 'styled-components';

interface Message {
  message: any,
  priority: any,
}

interface AppProps {
  msg: Message;
  clearRecord: Function
}


const ClearText = styled.small`
  position: absolute;
  flex: 1;
  bottom: 10px;
  z-index: 9999;
  right: 20px
`;

const CustomInfoTableRow = styled.div`
  margin: 10px;
  flex: 1;
  flex-basis: 410px;
  position: relative;
  background-color: #88FCA3;
  min-width: 300px;
`;

export default function InfoRow({ clearRecord, msg }: AppProps) {
  return (
    <CustomInfoTableRow  key={msg.message}>
      <TableRow>
        <TableCell component="th" scope="row">
          {msg.message}
        </TableCell>
      </TableRow>
      <ClearText onClick={() => clearRecord(msg)}>
        Clear
      </ClearText>
    </CustomInfoTableRow>
  )
}
