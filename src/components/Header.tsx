import React from 'react'
import styled from 'styled-components';

const HeaderDiv = styled.div`
  width: 100%;
  height: 30px;
  display: flex;
  align-items: center;
  padding-left: 40px;
  border: 1px solid black
`;


export default function Header() {
  return (
    <HeaderDiv>
        <strong> Coding Challenge </strong>
    </HeaderDiv>
  )
}
