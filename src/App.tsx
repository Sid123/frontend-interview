import React, { useState } from 'react';
import { useEffect } from 'react';
import generateMessage, { Message } from './Api';
import Messages from './components/Messages';
import SnakBar from './components/SnakBar';

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [stopMessages, setstopMessages] = useState(false)
  const [open, setOpen] = React.useState(false);
  const [ErrorMsg, setErrorMsg] = useState<any>({})

  useEffect(() => {
    const cleanUp = generateMessage((message: Message) => {
      if(message.priority === 0) {
        setErrorMsg(message)
        setOpen(true)
      }
      !stopMessages && setMessages(oldMessages => [...oldMessages, message]);
    });
    return cleanUp;
  }, [setMessages, stopMessages]);
  
  const clearMessages = () => {
    setMessages([])
  }

 

  return (
    <div>
      <Messages
        clearMessages={clearMessages}
        setstopMessages={() => setstopMessages(!stopMessages)}
        stopMessages={stopMessages}
        message={messages}
      />
      <SnakBar setOpen={setOpen} open={open} ErrorMsg={ErrorMsg}/>
    </div>
  );
}

export default App;
